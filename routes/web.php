<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     $barangs = Barang::paginate(20);
//     return view('shoppers.index', compact('barangs '));
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/pesan/{id}', 'PesanController@index');
Route::post('/pesan/{id}', 'PesanController@pesan');

Route::get('/checkout', 'PesanController@checkout');
Route::delete('checkout/{id}', 'PesanController@delete');

Route::get('/checkout/cetak', function () {
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Cetak</h1>');
    return $pdf->stream();
});

Route::get('/confirm', 'PesanController@confirm');

Route::get('/profile', 'ProfileController@index');
Route::post('/profile', 'ProfileController@update');

Route::get('/history', 'HistoryController@index');
Route::get('/history/{id}', 'HistoryController@detail');

Route::resource('menu', MenuController::class);
Route::resource('category', CategoryController::class);

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/excel', 'PostController@export');
