<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Pesanan;
use App\User;
use App\PesananDetail;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        $pesanan_details = [];
        $barangs = Barang::paginate(20);
        // return view('home', compact('barangs'));
        return view('shoppers.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barangs = DB::table('barangs')->get();
        return view('menu.create', compact('barangs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'menu_name' => 'required',
                'menu_price' => 'required|numeric',
                'menu_stock' => 'required|numeric',
                'menu_desc' => 'required',
                'image' => 'required|file|max:1200',
            ],
            [
                'menu_name.required' => 'Name can not be null!',
                'menu_price.required' => 'Price can not be null!',
                'menu_stock.required' => 'Stock can not be null!',
                'menu_desc.required' => 'Desc can not be null!',
                'image.required' => 'Image can not be null!',
                'image.max' => 'Image max size is 1mb!',
            ]
        );

        if ($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('');
        }

        DB::table('barangs')->insert([
            'nama_barang' => $request['menu_name'],
            'harga' => $request['menu_price'],
            'stok' => $request['menu_stock'],
            'gambar' => $validatedData['image'],
            'keterangan' => $request['menu_desc'],
            'created_at' => now(),
        ]);

        Alert::success('Create Successfull!', 'Success');
        return redirect('/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $barangs = DB::table('barangs')->where('id', $id)->first();
        return view('pesan.show', compact('barangs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $barangs = DB::table('barangs')->where('id', $id)->first();
        return view('pesan.edit', compact('barangs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('barangs')->where('id', $id)->update([
            'nama_barang' => $request['menu_name'],
            'harga' => $request['menu_price'],
            'stok' => $request['menu_stock'],
            'gambar' => 'default.jpg',
            'keterangan' => $request['menu_desc'],
            'updated_at' => now(),
        ]);
        Alert::success('Update Product Successfull!', 'Success');
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('barangs')->where('id', '=', $id)->delete();

        Alert::success('Delete Successfull!', 'Success');
        return redirect('/menu');
    }
}
