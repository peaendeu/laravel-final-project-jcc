<?php

namespace App\Exports;

use App\PesananDetail;
use App\Post;
use Maatwebsite\Excel\Concerns\FromCollection;

class PostsExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return PesananDetail::all();
    }
}
