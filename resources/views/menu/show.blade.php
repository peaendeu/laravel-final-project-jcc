@extends('layout.master')

@section('title', 'Detail Page')
@section('content')
<div class="card col-md-4">
  {{-- <img src="..." class="card-img-top" alt="..."> --}}
  <div class="card-body">
    <h5 class="card-title">{{ $menus->menu_name }}</h5>
    <p class="card-text">Price : Rp. {{ number_format($menus->menu_price) }}</p>
    <p class="card-text">Stock : {{ $menus->menu_stock }}</p>
    <a href="/menu" class="btn badge btn-outline-dark">Back</a>
  </div>
</div>
@endsection