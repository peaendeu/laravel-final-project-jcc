@extends('layout.master')

@section('title', 'Edit Page')
@section('content')
<form action="/menu" method="post" class="col-md-4">
  @csrf
  <div class="form-group">
    <label for="menu_name">Name</label>
    <input type="text" class="form-control @error('menu_name') is-invalid @enderror" id="menu_name" name="menu_name" aria-describedby="emailHelp" value="{{ $menus->menu_name }}">
    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
    @error('menu_name')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
    @enderror
  </div>
  <div class="form-group">
    <label for="menu_price">Price</label>
    <input type="text" class="form-control @error('menu_price') is-invalid @enderror" id="menu_price" name="menu_price" aria-describedby="emailHelp" value="{{ $menus->menu_price }}">
    @error('menu_price')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
    @enderror
  </div>
  <div class="form-group">
    <label for="menu_stock">Stock</label>
    <input type="text" class="form-control @error('menu_stock') is-invalid @enderror" id="menu_stock" name="menu_stock" aria-describedby="emailHelp" value="{{ $menus->menu_stock }}">
    @error('menu_stock')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
    @enderror
  </div>
  <button type="submit" class="btn btn-outline-dark">Submit</button>
</form>
@endsection