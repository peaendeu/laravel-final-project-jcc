@extends('layouts.app')

@section('title', 'Create Page')
@section('content')
<div class="container">
  <h1>Create a Product</h1>
  <form action="/menu" method="post" class="col-md-4" enctype="multipart/form-data">
    @csrf    
    <div class="form-group">
      <label for="menu_name">Name</label>
      <input type="text" class="form-control @error('menu_name') is-invalid @enderror" id="menu_name" name="menu_name" aria-describedby="emailHelp">
      
      @error('menu_name')
        {{-- <div class="alert alert-danger">{{ $message }}</div> --}}
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    <div class="form-group">
      <label for="menu_price">Price</label>
      <input type="text" class="form-control @error('menu_price') is-invalid @enderror" id="menu_price" name="menu_price" aria-describedby="emailHelp">
      @error('menu_price')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    <div class="form-group">
      <label for="menu_stock">Stock</label>
      <input type="text" class="form-control @error('menu_stock') is-invalid @enderror" id="menu_stock" name="menu_stock" aria-describedby="emailHelp">
      @error('menu_stock')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>

    <div class="form-group">
      <label for="image">Image</label>
      <input type="file" class="form-control-file @error('image') is-invalid @enderror" id="image" name="image">
      @error('image')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    
    <div class="form-group">
      <label for="menu_desc">Desc</label>
      {{-- <textarea name="menu_desc" id="menu_desc" cols="30" rows="10" class="form-control @error('menu_desc') is-invalid @enderror"></textarea> --}}
      <textarea name="menu_desc" id="menu_desc" class="form-control my-editor @error('menu_desc') is-invalid @enderror">{!! old('menu_desc', $content ?? '') !!}</textarea>
      {{-- <input type="text" class="form-control @error('menu_stock') is-invalid @enderror" id="menu_stock" name="menu_stock" aria-describedby="emailHelp"> --}}
      @error('menu_desc')
      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>

    <button type="submit" class="btn btn-outline-dark">Submit</button>
  </form>
</div>
@endsection