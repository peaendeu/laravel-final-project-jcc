@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card mb-3">
        <div class="card-body text-center">
          <h3>Thanks for your order.</h3>
          <h5>Now, You have to pay {{ number_format($pesanan->jumlah_harga+$pesanan->kode) }} to your shop account.</h5>
        </div>
      </div>
      <div class="card">
        <a href="/history" class="btn btn-light mb-3"><i class="fa fa-arrow-left"></i></a>
        <div class="card-body text-center">

          <h3><i class="fa fa-shopping-cart"></i> Order Detail</h3>

          @if(!empty($pesanan))
          <p>Order Date : {{ $pesanan->tanggal }}</p>
          <table class="table table-striped text-center">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Image</th>
                <th>Quantity</th>
                <th>Price (Rp.)</th>
                <th>Total (Rp.)</th>
              </tr>
            </thead>

            <tbody>
              <?php $no = 1; ?>
              @forelse ($pesanan_details as $pesanan_detail)
              <tr>
                <td>{{ $no++ }}</td>
                <td>
                  <img src="{{ url('storage') }}/{{ $pesanan_detail->barang->gambar }}" alt="{{ $pesanan_detail->barang->gambar }}" width="100">
                </td>
                <td>{{ $pesanan_detail->barang->nama_barang }}</td>
                <td>{{ $pesanan_detail->jumlah }}</td>
                <td>{{ number_format($pesanan_detail->barang->harga) }}</td>
                <td>{{ number_format($pesanan_detail->jumlah_harga) }}</td>
              </tr>                  
              @empty
                <h3>Your cart is empty.</h3>
              @endforelse
              <tr>
                <td colspan="3"></td>
                <td>Total Price<br>{{ number_format($pesanan->jumlah_harga) }}</td>                
                <td>Unique Code<br>{{ number_format($pesanan->kode) }}</td>                
                <td>Payment<br>{{ number_format($pesanan->jumlah_harga+$pesanan->kode) }}</td>
              </tr>
              
            </tbody>
          </table>
          @endif

        </div>
      </div>
    </div>    

  </div>
</div>
@endsection
