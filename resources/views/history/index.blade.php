@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">

        <a href="/" class="btn btn-light mb-3"><i class="fa fa-arrow-left"></i></a>

          <h3><i class="fa fa-history"></i> Order History</h3>
          
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Number</th>
                <th>Date</th>
                <th>Status</th>
                <th>Total Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              @foreach ($pesanans as $pesanan)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $pesanan->tanggal }}</td>
                  <td>
                    @if ($pesanan->status == 1)
                      Have been ordered & Payment Required
                    @else                        
                      Paid
                    @endif
                  </td>
                  <td>Rp. {{ number_format($pesanan->jumlah_harga+$pesanan->kode) }}</td>
                  <td>
                    <a href="/history/{{ $pesanan->id }}" class="btn btn-light"><i class="fa fa-info"></i> Detail</a>
                  </td>
                </tr>
              @endforeach              
            </tbody>
          </table>

        </div>
      </div>
    </div>    

  </div>
</div>
@endsection
