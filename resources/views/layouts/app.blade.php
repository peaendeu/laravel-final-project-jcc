<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>E-Commerce</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('shoppers/fonts/icomoon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/style.css') }}">

    <script src="/path-to-your-tinymce/tinymce.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{-- {{ config('app.name', 'Laravel') }} --}}
                    {{-- <img src="{{ url('img/Danpu.png') }}" alt="Danpu.png" width="100"> --}}
                    <a href="/" class="js-logo-clone text-dark">E-Commerce</a>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <li class="nav-item">
                            <?php 
                                $pesanan_utama = \App\Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
                                if(!empty($pesanan_utama)){
                                    $notif = \App\PesananDetail::where('pesanan_id', $pesanan_utama->id)->count(); 
                                }
                            ?>
                            <a class="nav-link" href="{{ url('checkout') }}"><i class="fa fa-shopping-cart"></i>
                                @if (!empty($notif))
                                    <span class="badge badge-danger">{{ $notif }}</span>                                    
                                @endif
                            </a>
                        </li>
                        <a class="dropdown-item">
                            {{ Auth::user()->name }} 
                        </a>
                        
                        <a class="dropdown-item" href="{{ url('profile') }}">
                            Profile
                        </a>

                        <a class="dropdown-item" href="{{ url('history') }}">
                            History
                        </a>

                        <a class="dropdown-item" href="{{ url('category') }}">
                            Category
                        </a>
                        
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                            <li class="nav-item dropdown">
                                

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                   
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @include('sweetalert::alert')
    <script src="{{ asset('shoppers/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('shoppers/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('shoppers/js/popper.min.js') }}"></script>
    <script src="{{ asset('shoppers/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('shoppers/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('shoppers/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('shoppers/js/aos.js') }}"></script>
  
    <script src="{{ asset('shoppers/js/main.js') }}"></script>
    <script>
        var editor_config = {
          path_absolute : "/",
          selector: 'textarea.my-editor',
          relative_urls: false,
          plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table directionality",
            "emoticons template paste textpattern"
          ],
          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          file_picker_callback : function(callback, value, meta) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
      
            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
            if (meta.filetype == 'image') {
              cmsURL = cmsURL + "&type=Images";
            } else {
              cmsURL = cmsURL + "&type=Files";
            }
      
            tinyMCE.activeEditor.windowManager.openUrl({
              url : cmsURL,
              title : 'Filemanager',
              width : x * 0.8,
              height : y * 0.8,
              resizable : "yes",
              close_previous : "no",
              onMessage: (api, message) => {
                callback(message.content);
              }
            });
          }
        };
      
        tinymce.init(editor_config);
      </script>
</body>
</html>
