@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">

    <div class="col-md-4">
      <a href="/" class="btn btn-light mb-3"><i class="fa fa-arrow-left"></i></a>
    </div>

    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <img src="{{ url('storage') }}/{{ $barang->gambar }}" alt="{{ $barang->gambar }}" width="100%" class="rounded mx-auto mb-3">
            </div>

            <div class="col-md-6">
              <h3>{{ $barang->nama_barang }}</h3>
              <table class="table">
                <tbody>
                  <tr>
                    <td>Price</td>
                    <td>:</td>
                    <td>Rp. {{ number_format($barang->harga) }}</td>
                  </tr>
                  <tr>
                    <td>Stock</td>
                    <td>:</td>
                    <td>{{ number_format($barang->stok) }}</td>
                  </tr>
                  <tr>
                    <td>Desc.</td>
                    <td>:</td>
                    <td>{{ $barang->keterangan }}</td>
                  </tr>                  
                  <tr>
                    <td><label for="jumlah_pesan">Total</label></td>
                    <td>:</td>
                    <td>
                      <form action="{{ url('pesan') }}/{{ $barang->id }}" method="post">
                        @csrf
                      <input type="number" name="jumlah_pesan" id="jumlah_pesan" class="form-control" required autofocus>   
                      <br>  
                      <button type="submit" class="btn btn-light"><i class="fa fa-shopping-cart"></i>Add to Cart</button>                  
                    </td>
                  </tr>
                  </form> 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
