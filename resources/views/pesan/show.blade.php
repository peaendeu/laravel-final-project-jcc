@extends('layouts.app')

@section('title', 'Detail Page')
@section('content')
<div class="container">
  <div class="card col-md-4">
    {{-- <img src="..." class="card-img-top" alt="..."> --}}
    <div class="card-body">
      <form action="/menu/{{ $barangs->id }}" method="post" >
        @csrf
        @method('delete')
      <img src="{{ url('storage') }}/{{ $barangs->gambar }}" width="300">
      <h5 class="card-title mt-3">{{ $barangs->nama_barang }}</h5>
      <p class="card-text">Price : Rp. {{ number_format($barangs->harga) }}</p>
      <p class="card-text">Stock : {{ $barangs->stok }}</p>
      <p class="card-text">Desc : {{ $barangs->keterangan }}</p>
      <input type="submit" class="btn btn-outline-dark" value="Delete" onclick="return confirm('Are you sure delete this data?')">
      <a href="/" class="btn btn-outline-dark">Back</a>
      <a href="/menu/{{ $barangs->id }}/edit" class="btn btn-outline-dark">Edit</a>
    </form>
    </div>
  </div>
</div>
@endsection