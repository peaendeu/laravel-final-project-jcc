@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">

        <a href="/" class="btn btn-light mb-3"><i class="fa fa-arrow-left"></i></a>

          <h3><i class="fa fa-shopping-cart"></i> Checkout</h3>
          @if(!empty($pesanan))
          <p>Order Date : {{ $pesanan->tanggal }}</p>
          <table class="table table-striped text-center">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Image</th>
                <th>Quantity</th>
                <th>Price (Rp.)</th>
                <th>Action</th>
                <th>Total (Rp.)</th>
              </tr>
            </thead>

            <tbody>
              <?php $no = 1; ?>
              @forelse ($pesanan_details as $pesanan_detail)
              <tr>
                <td>{{ $no++ }}</td>
                <td>
                  <img src="{{ url('storage') }}/{{ $pesanan_detail->barang->gambar }}" alt="{{ $pesanan_detail->barang->gambar }}" width="100">
                </td>
                <td>{{ $pesanan_detail->barang->nama_barang }}</td>
                <td>{{ $pesanan_detail->jumlah }}</td>
                <td>{{ number_format($pesanan_detail->barang->harga) }}</td>                
                <td>
                  <form action="/checkout/{{ $pesanan_detail->id }}" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <button type="submit" class="badge badge-danger" onclick="return confirm('Are you sure delete this item?')"><i class="fa fa-trash"></i></button>
                  </form>
                </td>
                <td>{{ number_format($pesanan_detail->jumlah_harga) }}</td>
              </tr>                  
              @empty
                <h3>Your cart is empty.</h3>
              @endforelse
              <tr>
                <td colspan="6"></td>
                <td>Total Price : {{ number_format($pesanan->jumlah_harga) }}</td>                
              </tr>
            </tbody>            
          </table>

          <div class="row justify-content-end mr-1">
            <a href="/confirm" class="btn btn-success">
              <i class="fa fa-shopping-cart"> Check Out</i>
            </a>
          </div>

          <div class="row justify-content-end mr-1">
            <a href="/checkout/cetak" class="btn btn-primary">
              <i class="fa fa-shopping-cart"> Print</i>
            </a>
          </div>

          <div class="row justify-content-end mr-1">
            <a href="/excel" class="btn btn-primary">
              <i class="fa fa-shopping-cart"> Excel</i>
            </a>
          </div>
          @endif          

        </div>
      </div>
    </div>    

  </div>
</div>
@endsection
