@extends('layouts.app')

@section('title', 'Edit Page')
@section('content')
<div class="container">
  <form action="/menu/{{ $barangs->id }}" method="post" class="col-md-4">
    @csrf
    @method('put')
    <div class="form-group">
      <img src="{{ url('img') }}/{{ $barangs->gambar }}" width="300">

      <label for="menu_name">Name</label>
      <input type="text" class="form-control @error('menu_name') is-invalid @enderror" id="menu_name" name="menu_name" aria-describedby="emailHelp" value="{{ $barangs->nama_barang }}">
      {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
      @error('menu_name')
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    <div class="form-group">
      <label for="menu_price">Price</label>
      <input type="text" class="form-control @error('menu_price') is-invalid @enderror" id="menu_price" name="menu_price" aria-describedby="emailHelp" value="{{ $barangs->harga }}">
      @error('menu_price')
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    <div class="form-group">
      <label for="menu_stock">Stock</label>
      <input type="text" class="form-control @error('menu_stock') is-invalid @enderror" id="menu_stock" name="menu_stock" aria-describedby="emailHelp" value="{{ $barangs->stok }}">
      @error('menu_stock')
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>

    <div class="form-group">
      <label for="menu_desc">Desc</label>
      <textarea name="menu_desc" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" cols="30" rows="10">{{ $barangs->keterangan }}</textarea>
      @error('keterangan')
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    <button type="submit" class="btn btn-outline-dark" value="put">Submit</button>
  </form>
</div>
@endsection