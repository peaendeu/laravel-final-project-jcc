@extends('layouts.app')

@section('title', 'Create Page')
@section('content')
<div class="container">
  <form action="/category" method="post" class="col-md-4" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="category">Category</label>
      <input type="text" class="form-control @error('category') is-invalid @enderror" id="category" name="category" aria-describedby="emailHelp">
      
      @error('category')
        {{-- <div class="alert alert-danger">{{ $message }}</div> --}}
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>
    <button type="submit" class="btn btn-outline-dark">Submit</button>
  </form>
</div>
@endsection