@extends('layouts.app')

@section('title', 'Detail Page')
@section('content')
<div class="container">
  <div class="card col-md-4">
    <div class="card-body">
      <h5 class="card-title">{{ $categories->id }}</h5>
      <p class="card-text">{{ $categories->name }}</p>
      <a href="/category" class="btn badge btn-outline-dark">Back</a>
    </div>
  </div>
</div>
@endsection