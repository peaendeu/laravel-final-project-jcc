@extends('layouts.app')

@section('title', 'Edit Page')
@section('content')
<div class="container">
  <form action="/category/{{ $categories->id }}" method="post" class="col-md-4">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="category">Category</label>
      <input type="text" class="form-control @error('category') is-invalid @enderror" id="category" name="category" aria-describedby="emailHelp" value="{{ $categories->name }}">
      {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
      @error('category')
        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
      @enderror
    </div>    
    <button type="submit" class="btn btn-outline-dark" value="put">Submit</button>
  </form>
</div>

@endsection