<!DOCTYPE html>
<html lang="en">
  <head>
    <title>E-Commerce</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('shoppers/fonts/icomoon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('shoppers/css/owl.theme.default.min.css') }}">

    <link rel="stylesheet" href="{{ asset('shoppers/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('shoppers/css/style.css') }}">
    
  </head>
  <body>
  
  <div class="site-wrap">
    <header class="site-navbar" role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
              <form action="" class="site-block-top-search">
                <span class="icon icon-search2"></span>
                <input type="text" class="form-control border-0" placeholder="Search">
              </form>
            </div>

            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="/" class="js-logo-clone">E-Commerce</a>
              </div>
            </div>

            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <form action="/logout" method="post">
                  @csrf
                  <li class="nav-item">
                    <?php 
                        // $pesanan_utama = \App\Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
                        if(!empty($pesanan_utama)){
                            $notif = \App\PesananDetail::where('pesanan_id', $pesanan_utama->id)->count(); 
                        }
                    ?>
                    <a class="nav-link" href="{{ url('checkout') }}"><i class="fa fa-shopping-cart"></i>
                        @if (!empty($notif))
                            <span class="badge badge-danger">{{ $notif }}</span>                                    
                        @endif
                    </a>
                  </li>
                  <li><a href="/profile">Profile</a></li>
                  <li><a href="/history">History</span></a></li>
                  <li><a href="/category">Category</span></a></li>
                  </form>
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      
    </header>    
    
  </div>

  <div class="container">
    <table class="table col-md-6">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Category</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
      @forelse ($categories as $key => $category)
        <tr>
          <th scope="row">{{ $key+1 }}</th>
          <td>{{ $category->name }}</td>
          <td>
            <form action="/category/{{ $category->id }}" method="post">
              @csrf
              @method('delete')
              <a href="/category/{{ $category->id }}" class="btn btn-outline-dark">Detail</a>
              <a href="/category/{{ $category->id }}/edit" class="btn btn-outline-dark">Edit</a>
              <input type="submit" class="btn btn-outline-dark" value="Delete" onclick="return confirm('Are you sure delete this data?')">
            </form>
          </td>
        </tr>
      @empty
      <tr>
        <th colspan="3" class="text-center">
          <h3>Table is empty.</h3>
        </th>
      </tr>
      @endforelse
      </tbody>
    </table>
        
  </div>

  <script src="{{ asset('shoppers/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('shoppers/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('shoppers/js/popper.min.js') }}"></script>
  <script src="{{ asset('shoppers/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('shoppers/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('shoppers/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('shoppers/js/aos.js') }}"></script>

  <script src="{{ asset('shoppers/js/main.js') }}"></script>
    
  </body>
</html>