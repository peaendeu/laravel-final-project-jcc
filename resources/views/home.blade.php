@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($barangs as $barang)
        <div class="col-md-4">
            <div class="card">
                <img src="{{ url('img') }}/{{ $barang->gambar }}" class="card-img-top" alt="{{ $barang->gambar }}" width="40">
                <div class="card-body">
                <h5 class="card-title">{{ $barang->nama_barang }}</h5>
                <p class="card-text">Rp. {{ number_format($barang->harga) }}</p>
                <p class="card-text">Stock : {{ number_format($barang->stok) }}</p>
                <hr>
                <p class="card-text text-muted">{{ ($barang->keterangan) }}</p>
                <a href="/pesan/{{ $barang->id }}" class="btn btn-light"><i class="fa fa-shopping-cart"></i> Buy</a>
                </div>
            </div>
        </div>
        @empty
        <h3>Data is empty.</h3>
        @endforelse
    </div>
</div>
@endsection
